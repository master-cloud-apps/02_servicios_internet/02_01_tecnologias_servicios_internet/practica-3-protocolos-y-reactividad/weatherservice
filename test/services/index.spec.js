const { expect } = require('chai')

const getCityWeather = require('./../../src/services/index.js')

describe('Return weather service', () => {
  it('Given vowel city should return Rainy', () => {
    return getCityWeather({ city: 'Alava' })
      .then(weather => expect(weather).to.be.equal('Rainy'))
  }).timeout(3200)
  it('Given consonant city should return Sunny', () => {
    return getCityWeather({ city: 'Madrid' })
      .then(weather => expect(weather).to.be.equal('Sunny'))
  }).timeout(3200)
})
