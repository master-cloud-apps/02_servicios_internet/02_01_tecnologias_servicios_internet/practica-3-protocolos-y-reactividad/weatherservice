const { getWeather } = require('./../test/client/index.js')

getWeather({ city: 'San Sebastian' })
  .then(response => console.log('Response for San Sebastian: ' + JSON.stringify(response)))

getWeather({ city: 'Alava' })
  .then(response => console.log('Response for Alava: ' + JSON.stringify(response)))
